import { useState } from "react";
import { Row, Col, Modal, Button, Form } from "react-bootstrap";

const ModalForm = ({ show, onHide, setCalendarState, calendarState, setModalShow, id, setId }) => {

    const [errorMessage, setErrorMessage] = useState('Fill the inputs first.');
    const [bookingInfo, setBookingInfo] = useState({
        id,
        name: {
            value: "",
            valid: false,
            validate: (value) => value.length > 0 && /^[A-Za-z0-9 ]+$/.test(value),
            errMessage: 'Username must be atleast one character and not include special ones.'
        },
        startingDate: {
            value: "",
            valid: false,
            validate: (value) => /^\d{4}-\d{1,2}-\d{1,2}$/.test(value),
            errMessage: 'Starting date is not the right format.'
        },
        endingDate: {
            value: "",
            valid: false,
            validate: (value) => /^\d{4}-\d{1,2}-\d{1,2}$/.test(value) && value > bookingInfo.startingDate.value,
            errMessage: 'Ending date is not the right format or is before the starting date.'
        }
    });

    const handleFormChange = event => {

        bookingInfo[event.target.name].value = event.target.value;
        bookingInfo[event.target.name].valid = bookingInfo[event.target.name].validate(event.target.value);

        if(bookingInfo[event.target.name].valid === false) {
            setErrorMessage(bookingInfo[event.target.name].errMessage);
        }

        if(event.target.name === "startingDate") {
            bookingInfo.endingDate.valid = bookingInfo.endingDate.validate(bookingInfo.endingDate.value);
        }

        const updatedBookingInfo = {...bookingInfo};

        setBookingInfo(updatedBookingInfo);
    }

    const handleSubmit = event => {
        event.preventDefault();

        if(Object.values(bookingInfo).filter(el => typeof el === "object").some(el => el.valid === false)) {
            alert(errorMessage);
            return;
        }

        //check if selected dates are available
        const isEveryDateAvailable = calendarState.filter(el => el.date >= bookingInfo.startingDate.value && el.date <= bookingInfo.endingDate.value)
        .every(el => el.status === "Available");

        if(!isEveryDateAvailable) {
            alert("The dates you selected are unavailable.");
            return;
        }

        const newCalendarState = calendarState.map(el => {
            if ( el.date >= bookingInfo.startingDate.value && el.date <= bookingInfo.endingDate.value) {
                return {
                    id,
                    date: el.date,
                    status: "Booked",
                    price: el.price,
                    bookedBy: bookingInfo.name.value
                }
            } else {
                return el;
            }
        })        

        setId(id+1);
        setCalendarState(newCalendarState);
        setModalShow(false);
    }

    return (
        <Modal
            show={show} onHide={onHide}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
        >
            <Modal.Header closeButton>
                <Modal.Title id="contained-modal-title-vcenter">
                    Add reservation info
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Row>
                    <Form onSubmit={handleSubmit} >
                        <Row style={{marginLeft: "0px"}}>
                            <Form.Label>Guest name</Form.Label>
                            <Form.Control name="name" placeholder="Name" type="text" value={bookingInfo.name.value} onChange={handleFormChange}/>
                        </Row>
                        <Row>
                            <Col>
                                <Form.Label>From:</Form.Label>
                                <Form.Control name="startingDate" type="date" onChange={handleFormChange} />
                            </Col>
                            <Col>
                                <Form.Label>To:</Form.Label>
                                <Form.Control name="endingDate" type="date" onChange={handleFormChange} />
                            </Col>
                        </Row>
                        <Button variant="primary" type="submit" >Add</Button>
                    </Form>
                </Row>
            </Modal.Body>
        </Modal>
    );
}

export default ModalForm;