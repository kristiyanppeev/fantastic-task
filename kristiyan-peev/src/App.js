import logo from './logo.svg';
import './App.css';
import { Button, Container, Card, Row } from 'react-bootstrap';
import { useEffect, useState } from 'react';
import ModalForm from './ModalForm';
import Table from 'react-bootstrap/Table'

function App() {
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);

  const [calendarState, setCalendarState] = useState(null);
  const [calendarStateElements, setCalendarStateElements] = useState(null);
  const [modalShow, setModalShow] = useState(false);
  const [id, setId] = useState(0);
  const [currentReservations, setCurrentReservations] = useState(null);

  useEffect(() => {
    setLoading(true);
    fetch("./calendar.json")
      .then(res => res.json())
      .then(res => {

        const formattedResponse = res.map(el => {
          return {
            id: null,
            date: el.date,
            status: el.status === "blocked" ? el.status.charAt(0).toUpperCase() + el.status.slice(1) : el.status === "bookable" ? "Available" : el.satus,
            bookedBy: null,
            price: el.price
          }
        })
        setCalendarState(formattedResponse);

      })
      .catch(err => setError(err.message))
      .finally(() => setLoading(false));
  }, [])

  useEffect(() => {
    if (calendarState) {
      const calendarStateMapped = calendarState.map((el, index) => {

        return (
          <Card
            bg={el.status === "Available" ? "success" : el.status === "Blocked" ? "danger" : "info"}
            key={index}
            style={{ width: '18rem', display: 'inline-block', margin: '5px' }}
            className="mb-2"
          >
            <Card.Header>{el.date}</Card.Header>
            <Card.Body>
              <Card.Title>{el.status}</Card.Title>
              {el.bookedBy && <Card.Text>by {el.bookedBy}</Card.Text>}
              <Card.Text>Price: {el.price}</Card.Text>
            </Card.Body>
          </Card>
        )
      })

      setCalendarStateElements(calendarStateMapped);
    }
  }, [calendarState])

  useEffect(() => {
    if (calendarState) {
      // in this chain, we first filter only booked days and then implement GroupBy to differentitate each reservations
      const grouppedReservations = calendarState.filter(el => el.status === "Booked").reduce((acc, el) => {
        if (acc[el.id] === undefined) {
          acc[el.id] = [el];
        } else {
          acc[el.id] = [...acc[el.id], el]
        }

        return acc;
      }, {})

      // in this map, we remove all the days of reservation except the first and the last day,
      // to use the dates afterwards in the reservations table
      const reservations = Object.values(grouppedReservations).map((reserv, index) => {
        return reserv.filter((days, index) => index === 0 || index === reserv.length - 1)
      })

      const reservationsElements = reservations.map((el, index) => {
        return (
          <tr key={index}>
            <td>{index + 1}</td>
            <td>{el[0].bookedBy}</td>
            <td>{el[0].date}</td>
            <td>{el[1].date}</td>
          </tr>
        )
      })

      setCurrentReservations(reservationsElements);


    }
  }, [calendarState])

  if (error) {
    return (
      <div>
        There was an error.
      </div>
    )
  }

  if (loading) {
    return (
      <div>
        Loading...
      </div>
    )
  }

  return (
    <Container className="App" style={{ maxWidth: '70%' }}>
      {modalShow ?
        <ModalForm
          show={modalShow}
          onHide={() => setModalShow(false)}
          setCalendarState={setCalendarState}
          calendarState={calendarState}
          setModalShow={setModalShow}
          id={id}
          setId={setId} />
        : null}
      <Row>
        Current reservations:
        {currentReservations && currentReservations.length !== 0 ?
          <Table striped bordered hover variant="dark">
            <thead>
              <tr>
                <th>#</th>
                <th>Guest</th>
                <th>From:</th>
                <th>To:</th>
              </tr>
            </thead>
            <tbody>
              {currentReservations}
            </tbody>
          </Table> : "There aren't current reservations"
        }
      </Row>
      <Row>
        {calendarStateElements}
      </Row>
      <Row>
        <Button onClick={() => setModalShow(true)}>Add Reservation</Button>
      </Row>
    </Container>
  );
}

export default App;
